FROM ubuntu:22.04

RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y curl php \
    && rm -rf /var/lib/apt/lists/*

RUN curl https://getcomposer.org/download/2.7.7/composer.phar -o /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer

# 'latest' issues a 302 redirect so we need to be sure to follow with -L
RUN curl -s -L https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -o /usr/bin/yq \
    && chmod +x /usr/bin/yq
