Usage
=====

1. Ensure you have a deploy token. This might be at the group or project level.

For a group deploy token, go to the group page, click Settings in the left bar,
go to repository > deploy tokens. Create a new token with the
write_package_registry permission and make a note of the token.

2. Provide the deploy token as a CI variable.

Go to the group page, click Settings in the left bar, go to CI/CD > variables.
Add a variable named DEPLOY_TOKEN with the value set to the token created
previously. Ensure you set it as a masked variable.

3. Ensure your project has a composer.json. For example,

{
    "name": "my_group_name/my_project_name",
    "description": "Drupal Theme Name",
    "type": "drupal-theme",
    "authors": [
        {
            "name": "Adam Thorn",
            "email": "support@ch.cam.ac.uk"
        }
    ],
    "require": {}
}

4. Include the CI config from this repository in the project you wish to deploy.

Create .gitlab-ci.yml and ensure it includes:

include:
  - project: 'ch/co/drupal-9/drupal9_deploy_composer'
    file: '/templates/.gitlab-ci-composer-deploy.yml'

Deploying new versions
======================

The CI template provided will publish a new composer package whenever a new tag
is pushed. You need to make sure for yourself that the 'version' in
modulename.info.yml matches the tag, or the CI job will deliberately fail.

I'm experimenting with the workflow but a possible strategy might be to have a
rolling "dev" version which then becomes the released package in due course.
For example,

# commit some work, then..
git tag 9.0.5-dev -m 'My commit message'
git push --tags

# do some more work which you want to be the updated dev release, then..
git tag -d 9.0.5-dev
git push --delete origin 9.0.5-dev
git tag 9.0.5-dev -m 'My newer commit message'
git push --tags

# etc etc, until we want a non-dev release
git tag -d 9.0.5-dev
git push --delete origin 9.0.5-dev
git tag 9.0.5 -m 'Releasing 9.0.5'
git push --tags
